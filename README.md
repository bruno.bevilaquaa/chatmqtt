## Instruções para rodar a aplicação:

### Utilizando docker-compose:
No diretório do projeto:
```sh
docker-compose up --build chat
```

### Instalação padrão:
Instalando o broker mosquitto:
```sh
sudo apt-get install mosquitto.
```
Instalando Node.js com nvm:
```sh
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```
```sh
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
```
```sh
nvm install node
```
```sh
nvm use node
```
No diretório do projeto:
```sh
mosquitto -c mosquitto.conf
```
```sh
npm install
```
```sh
npm run start
```
### A aplicação será iniciada em [http://localhost:3000](http://localhost:3000).

## Instruções de uso:
- Cada guia do chat aberta no navegador conectará um novo usuário.
- Para verificar os usuário online, clicar no ícone de usuários.
- Para acessar as conversas iniciadas, clicar no ícone balão de mensagem.
- Para criar um grupo, digite o nome no input "inicie uma conversa" e clique no ícone de criar grupo.
- Para iniciar uma conversa privada, digite o ID de usuário no input "inicie uma conversa", e tecle enter. O ID pode ser encontrado na tela de usuários online.