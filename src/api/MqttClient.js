import Paho from 'paho-mqtt';
import { v4 as uuidv4 } from 'uuid';

const EventHandler = new EventTarget();

const user = {
    id: `${uuidv4().split('-')[0]}`
}

let chatList = [];
let groupList = [];
let onlineUsers = [];

const userControlTopic = user.id + '_control';

const chatExists = (chatID) => {
    const participants = chatID.split('_');

    if(participants.length === 2){
        for(let chat of chatList) {
            if(chat.id.includes(participants[0]) && chat.id.includes(participants[1])) {
                return true;
            }
        }
    }

    return false;
}

const userIsOnGroup = (groupID) => {
    for(let chat of chatList) {
        if(chat.id === groupID) {
            return true;
        }
    }

    return false;
}

const getGroup = (groupID) => {
    for(let group of groupList) {
        if(group.id === groupID) {
            return group;
        }
    }

    return false;
}

const updateGroup = (groupData) => {
    for(let index in groupList) {
        if(groupList[index].id === groupData.id) {
            groupList[index] = groupData;
        }
    }
}

const onMessageArrived = (message) => {
    try {
        const receivedData = JSON.parse(message.payloadString);
        const receivedTopic = message.topic;

        if(receivedTopic === userControlTopic){
            if(receivedData.action === 'REQUEST' && receivedData.target === 'CONVERSATION') {
                const newChatID = `${receivedData.user.id}_${user.id}`;

                if(!chatExists(newChatID)) {
                    const newChat = {
                        type: 'private', 
                        id: newChatID, 
                        chatName: receivedData.user.id, 
                        lastMessage: receivedData.body.message, 
                        messages: [
                            {
                                sender: receivedData.user.id, 
                                body: receivedData.body.message
                            }
                        ]
                    };
                    
                    client.subscribe(newChatID, {qos: 2});

                    chatList = [...chatList, newChat];
    
                    EventHandler.dispatchEvent(new Event('conversationRequest'));
                }
            }
            else if(receivedData.action === 'JOIN'){
                for(let group of groupList) {
                    if(group.id === receivedData.groupID) {
                        group.users.push(receivedData.userToJoin);
                        
                        let messageToSend = new Paho.Message(JSON.stringify(group));
                        messageToSend.destinationName = 'GROUPS';
                        messageToSend.retained = true;
                        messageToSend.qos = 2;
                        client.send(messageToSend);

                        break;
                    }
                }                
            }
        }
        else if (receivedTopic === "USERS") {
            if(receivedData.action === "GET") {
                const message = {
                    action: "POST",
                    user
                }
                
                let messageToSend = new Paho.Message(JSON.stringify(message));
                messageToSend.destinationName = "USERS";
                messageToSend.qos = 2;
                client.send(messageToSend);
            }
            else if (receivedData.action === "POST") {
                let pushUser = true;

                for(let onlineUser of onlineUsers) {
                    if(onlineUser.id === receivedData.user.id){
                        pushUser = false;
                    }
                }

                if(pushUser && receivedData.user.id !== user.id) {
                    onlineUsers.push(receivedData.user);
                    EventHandler.dispatchEvent(new Event('onlineUsers'));
                }
            }
        }
        else if(receivedTopic === "GROUPS") {
            console.log(receivedData);

            const group = getGroup(receivedData.id);

            if(group) {
                updateGroup(receivedData);
            }
            else {
                groupList.push(receivedData);
            }         
            
            for(let groupUser of receivedData.users) {
                if(groupUser.id === user.id && !userIsOnGroup(receivedData.id)) {
                    chatList.push(receivedData);
                    client.subscribe(receivedData.id, {qos: 2});
                    EventHandler.dispatchEvent(new Event('conversationRequest'));
                    break;
                }
            }
        }
        else {
            for(let index in chatList) {
                if(receivedTopic === chatList[index].id) {
                    const messages = chatList[index].messages;
                    const newMessage = {
                        sender: receivedData.user.id, 
                        body: receivedData.body,
                        timestamp: receivedData.timestamp
                    }
                    chatList[index].lastMessage = receivedData.body;
                    chatList[index].messages.push(newMessage);
                    
                    EventHandler.dispatchEvent(new Event('newMessage'), messages);

                    break;
                }
            }
        }
       
    }
    catch(e){
        console.log("[ERROR][ONMESSAGEARRIVED] ", e);
    }
}

const client = new Paho.Client('127.0.0.1', 8883, user.id);
client.connect({
    onSuccess: () => {
        client.onMessageArrived = onMessageArrived;
        client.subscribe(`${user.id}_control`, {qos: 2});
        client.subscribe('USERS', {qos: 2});
        client.subscribe('GROUPS', {qos: 2});

        MqttClient.getOnlineUsers();
    }
});

const MqttClient = {
    publish: (topic, message, retained = false) => {
        let messageToSend = new Paho.Message(JSON.stringify(message));
        messageToSend.destinationName = topic;
        messageToSend.retained = retained;
        messageToSend.qos = 2;
        client.send(messageToSend);
    },

    subscribe: (topic) => {
        client.subscribe(topic);
    },

    getOnlineUsers: () => {
        MqttClient.publish("USERS", {action: "GET"});
    },

    createGroup: (groupName) => {
        const group = getGroup(groupName);
        
        if(group) {
            const joinRequest = {
                action: 'JOIN',
                groupID: groupName,
                userToJoin: user
            }

            MqttClient.publish(`${group.leader.id}_control`, joinRequest);
        }
        else {
            const newGroup = {
                type: 'group', 
                id: groupName, 
                chatName: groupName, 
                lastMessage: 'Grupo criado!', 
                messages: [
                   
                ],
                users: [
                    user
                ],
                leader: user
            };
        
            MqttClient.subscribe(groupName, {qos: 2});
            MqttClient.publish('GROUPS', newGroup, true);
        }
    },

    sendChatRequest: (targetUserID, setChatList) => {
        const newChatID = `${user.id}_${targetUserID}`;
        if(!chatExists(newChatID)) {
            const newChat = {
                type: 'private', 
                id: newChatID, 
                chatName: targetUserID, 
                lastMessage: `Olá ${targetUserID}, quero me conectar!`, 
                messages: [
                    {
                        sender: user.id, 
                        body: `Olá ${targetUserID}, quero me conectar!`
                    }
                ]
            }
            
            chatList = [...chatList, newChat];    
    
            const targetUserControlTopic = `${targetUserID}_control`;
            const requestChat = {
                user,
                action: 'REQUEST',
                target: 'CONVERSATION',
                body: {
                    message: `Olá ${targetUserID}, quero me conectar!`
                }
            };

            MqttClient.subscribe(newChatID);
            MqttClient.publish(targetUserControlTopic, requestChat)
            setChatList(chatList);
        }
    },

    getUser: () => {
        return user;
    },

    onOnlineUsers: (setOnlineUsers) => {
        return EventHandler.addEventListener('onlineUsers', () => {
            setOnlineUsers(onlineUsers);
        });
    },

    onConversationRequest: (setChatList) => {
        return EventHandler.addEventListener('conversationRequest', () => {
            setChatList(chatList);
        });
    },

    onCreatedGroups: (setUsersGroupList) => {
        return EventHandler.addEventListener('createdGroups', () => {
            setUsersGroupList(groupList);
        })
    },

    onNewMessage: (setMessages) => {
        return EventHandler.addEventListener('newMessage', (messages) => {
            setMessages(messages);
        });
    }
}

export default MqttClient;