import React, { useState, useEffect } from 'react';
import './style.css';
import NewChatIcon from '@material-ui/icons/AddComment';
import GroupIcon from '@material-ui/icons/Group';
import ChatIcon from '@material-ui/icons/Chat';
import CreateGroup from '@material-ui/icons/GroupAdd';

import ChatListItem from '../ChatListItem';
import OnlineListItem from '../UserListItem';
import ChatWindow from '../ChatWindow';
import ChatInformation from '../ChatInformation';

import MqttClient from '../../api/MqttClient';

const App = () => {
	const [chatList, setChatList] = useState([]);
	const [onlineUsers, setOnlineUsers] = useState([]);  	
	const [activeChat, setActiveChat] = useState({});
	const [activeHeaderButton, setActiveHeaderButton] = useState('chats');
	const [user, setUser] = useState(null);
	const [searchInput, setSearchInput] = useState('');
	const [showChatInformation, setShowChatInformation] = useState(false);

	useEffect(() => {
		if(!user) {
			setUser(MqttClient.getUser());
		}
		else {
			let unsub = MqttClient.onConversationRequest(setChatList);
			return unsub;
		}
	}, [user]);

	useEffect(() => {
		MqttClient.onOnlineUsers(setOnlineUsers);

	}, [onlineUsers]);

	const handleSubmit = (e) => {
		if(e.which === 13) {
			MqttClient.sendChatRequest(searchInput, setChatList);
			setSearchInput('');
		}
	}
	
	const handleOnlineUsersButton = async () => {
		MqttClient.getOnlineUsers();
		setActiveHeaderButton('onlineUsers');
	}

	const handleChatButton = () => {
		setActiveHeaderButton('chats');
	}

	const handleGroupButton = () => {
		MqttClient.createGroup(searchInput);
	}

	const handleActiveChat = (chat) => {
		setActiveChat(chat);
		setShowChatInformation(false);
	}


	return(
		<div className="app-window">
			<div className="sidebar">
				<header>
					<div className="header--user">
						<img className="header--avatar" src="https://png.pngtree.com/png-vector/20190710/ourlarge/pngtree-user-vector-avatar-png-image_1541962.jpg" alt=""/>
						<p>{user && user.id}</p>
					</div>
					<div className="header--buttons">
						<div className="header--btn">
							<GroupIcon 
								onClick={() => handleOnlineUsersButton()}
							/>
						</div>
						<div className="header--btn">
							<ChatIcon 
								onClick={() => handleChatButton()}
							/>
						</div>
						<div className="header--btn">
							<CreateGroup
								onClick={() => handleGroupButton()}
							/>
						</div>
						
					</div>
				</header>

				<div className="search"> 
					<div className="search--input">
						<NewChatIcon fontSize="small" style={{color: '#161B22'}}/>
						<input
							type="search" 
							placeholder="Inicie uma conversa"
							value={searchInput}
							onChange={e => setSearchInput(e.target.value)}
							onKeyPress={handleSubmit}
						/>
					</div>
				</div>

				<div className="chatlist">
					{
						activeHeaderButton === 'chats' ?	
						chatList.map((item, key) => {
							return (
								<ChatListItem 
									key={key}
									data={item}
									active={activeChat.id === item.id}
									onClick={() => handleActiveChat(item)}
								/>
							);
						})

						:
						
						onlineUsers.map((item, key) => {
							return (
								<OnlineListItem 
									key={key}
									user={item}
								/>
							);
						})						
					}
				</div>
			</div>

			<div className="contentarea">
				{
					activeChat.id !== undefined &&
					<ChatWindow
						user={user}
						chatData={activeChat}
						MqttClient={MqttClient}
						onClick={() => setShowChatInformation(true)}
					/>
				}
			</div>
			
			{
				showChatInformation &&
				<ChatInformation
					data={activeChat}
					onClick={() => setShowChatInformation(false)}
				/>
			}
		</div>
	);
}

export default App;