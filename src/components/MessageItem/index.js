import React from 'react';
import "./style.css";

const MessageItem = ({data, user, chatType}) => {
    return (
        <div 
            className="messageLine"
            style={{
                justifyContent: user.id === data.sender  ? 'flex-end' : 'flex-start'
            }}
        >
            <div className="messageItem">
                {
                    chatType === 'group' && user.id !== data.sender && 
                    <div className="messageSender">{data.sender}</div>
                }
                <div className="messageText">{data.body}</div>
                <div className="messageData">{data.timestamp || ''}</div>
            </div>
        </div>
    );
}

export default MessageItem;