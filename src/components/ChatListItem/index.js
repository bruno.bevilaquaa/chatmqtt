import React from 'react';
import "./style.css";

const ChatListItem = ({onClick, active, data}) => {
	return (
		<div 
			className={`chatListItem ${active ? 'active' : ''}`}
			onClick={onClick}
		>
			<img 
				className="chatListItem--avatar" 
				src={
					data.type === 'private' ? 
					'https://png.pngtree.com/png-vector/20190710/ourlarge/pngtree-user-vector-avatar-png-image_1541962.jpg' :
					'https://www.pinclipart.com/picdir/middle/21-218603_illustration-of-a-group-of-people-user-account.png'
				}
			/>
			<div className="chatListItem--lines">
				<div className="chatListItem--line">
					<div className="chatListItem--name">
						{data.chatName}
					</div>
				</div>
				<div className="chatListItem--line">
					<div className="chatListItem--lastMsg">
						<p>{data.lastMessage}</p>
					</div>
				</div>
			</div>
		</div>
	);
}

export default ChatListItem;