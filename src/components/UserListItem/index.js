import React from 'react';
import "./style.css";

const UserListItem = ({user, information}) => {
	return (
		<div className="userListItem">
			<img className="userListItem--avatar" src="https://png.pngtree.com/png-vector/20190710/ourlarge/pngtree-user-vector-avatar-png-image_1541962.jpg" alt=""/>
			<div className="userListItem--lines">
				<div className="userListItem--line">
					<div className="userListItem--user">
						{user.id}
						<div className={information ? "userListItem--information" : "userListItem--status"}>
							{information}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default UserListItem;