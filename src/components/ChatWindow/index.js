import React, { useEffect, useState, useRef } from 'react';
import "./style.css";
import SendIcon from '@material-ui/icons/Send';
import MessageItem from '../MessageItem';

const ChatWindow = ({user, chatData, MqttClient, onClick}) => {
	const body = useRef();
	
	const [inputText, setInputText] = useState('');
	const [messages, setMessages] = useState([]);

	useEffect(() => {
		if(body.current.scrollHeight > body.current.offsetHeight) {
			body.current.scrollTop = body.current.scrollHeight - body.current.offsetHeight;
		}

		let unsub = MqttClient.onNewMessage(setMessages);
		return unsub;

	}, [messages]);

	const handleSendEvent = () => {
		const date = new Date(); 

		const message = {
			user,
			body: inputText,
			timestamp: `${date.getHours()}:${date.getMinutes()}` 
		}
		
		MqttClient.publish(chatData.id, message);
		setInputText('');
	};

	const handleEnterEvent = (e) => {
		if(e.which === 13) {
			handleSendEvent();
		}
	}

	return (
		<div className="chatWindow">
			<div className="chatWindow--header">
				<div 
					className="chatWindow--headerinfo"
					onClick={chatData.type !== 'private' && onClick}
				>
					<img 
						className="chatWindow--avatar" 
						src={
							chatData.type === 'private' ? 
							'https://png.pngtree.com/png-vector/20190710/ourlarge/pngtree-user-vector-avatar-png-image_1541962.jpg' :
							'https://www.pinclipart.com/picdir/middle/21-218603_illustration-of-a-group-of-people-user-account.png'
						}
					/>
					<div className="chatWindow--name">{chatData.chatName}</div>

				</div>
			</div>
			<div ref={body} className="chatWindow--body">
				{
					chatData.messages.map((item, key) => {	
						return (
							<MessageItem
								data={item}
								user={user}
								chatType={chatData.type}
								key={key}
							/>
						);
					})
					
				}
			</div>
			<div className="chatWindow--footer">
				<div className="chatWindow--inputarea">
					<input 
						className="chatWindow--input" 
						type="text"
						placeholder="Digite uma mensagem..."
						value={inputText}
						onChange={e => setInputText(e.target.value)}
						onKeyPress={handleEnterEvent}
					/>
				</div>
				<div
					onClick={handleSendEvent}
					className="chatWindow--send"
				>
					<div className='chatWindow--btn'>
						<SendIcon/>
					</div>
				</div>
			</div>
		</div>
	);
}

export default ChatWindow;