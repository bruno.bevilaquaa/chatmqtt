import React from 'react';
import "./style.css";

import UserListItem from "../UserListItem";


const ChatInformation = ({data, onClick}) => {
    return(
        <div className="chatInformation">
            <div className="chatInformation--header">
                <h2 
                    className="chatInformation--close"
                    onClick={onClick}
                >
                    x
                </h2>
                <p className="chatInformation--header--text">Usuários do grupo</p>
            </div>

            {
                data.users &&
                data.users.map((item, key) => {
                    return (
                        <UserListItem 
                            key={key}
                            user={item}
                            information={item.id === data.leader.id ? "[Líder]" : "[Membro]"}
                        />
                    );
                })
            }
        </div>
    );
}

export default ChatInformation;